package ch.viseon.orca2.example.server.services


typealias TagId = Long

//FIXME Unit Test
class TagService : ITagService {

  private var idFake = 0L

  private val tags = mutableMapOf<TagId, Tag>()

  override fun addTag(tagName: String): TagId {
    ++idFake
    tags[idFake] = Tag(idFake, tagName)
    return idFake
  }

  override fun getTags(): List<Tag> {
    return tags.values.toList()
  }

  override fun deleteTag(tagId: TagId) {
    tags.remove(tagId)
  }

}

data class Tag(val id: TagId, val name: String)