package ch.viseon.orca2.example.server.services

interface ITagService {

  fun addTag(tagName: String): TagId
  fun getTags(): List<Tag>
  fun deleteTag(tagId: TagId)
}