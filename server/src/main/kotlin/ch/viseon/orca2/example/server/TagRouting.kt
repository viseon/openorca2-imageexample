package ch.viseon.orca2.example.server

import ch.viseon.orca2.example.dto.AddTagRequest
import ch.viseon.orca2.example.dto.AddTagResponse
import ch.viseon.orca2.example.dto.Tag
import ch.viseon.orca2.example.server.services.ITagService
import ch.viseon.orca2.example.server.services.ServiceRegistry
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receiveText
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import kotlinx.serialization.ImplicitReflectionSerializer
import kotlinx.serialization.json.JSON
import kotlinx.serialization.list
import kotlinx.serialization.serializer


fun Routing.tagApi() {

  val tagService = ServiceRegistry[ITagService::class]

  post("/tag") {
    val requestData = JSON.parse(AddTagRequest.serializer(), call.receiveText())
    val tagId = tagService.addTag(requestData.tagName)
    val responseString = JSON.stringify(AddTagResponse.serializer(), AddTagResponse(tagId))
    call.respond(HttpStatusCode.OK, responseString)
  }
  get("/tag") {
    val dtoTags = tagService.getTags()
            .map { Tag(it.id, it.name) }

    val serializer = Tag.serializer().list
    val responseString = JSON.stringify(serializer, dtoTags.toList())
    call.respond(HttpStatusCode.OK, responseString)
  }
  delete("/tag/{tagId}") {
    val tagId = call.parameters["tagId"]
    tagId?.let {
      if (tagId.isEmpty()) {
        call.respond(HttpStatusCode.BadRequest, "TagId not provided")
      } else {
        tagService.deleteTag(it.toLong())
        call.respond(HttpStatusCode.OK, "")
      }
    } ?: call.respond(HttpStatusCode.BadRequest, "TagId not provided")
  }
}
