package ch.viseon.orca2.example.server

import ch.viseon.orca2.example.server.services.*
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.http.HttpMethod
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty

fun main(args: Array<String>) {

  ServiceRegistry.registerServices {
    register(ITagService::class, TagService())
    register(IFileService::class, FileService())
  }

  val server = embeddedServer(Netty, port = 8080) {

    install(CORS) {
      allowCredentials = true
      method(HttpMethod.Delete)
      anyHost() //FIXME Change to something minigfull
    }

    routing {
      tagApi()
      pictureApi()
    }
  }
  server.start(wait = true)
}

