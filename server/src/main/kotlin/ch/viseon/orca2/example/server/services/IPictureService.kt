package ch.viseon.orca2.example.server.services


interface IFileService {
  fun getFiles(): Collection<Map.Entry<String, PictureHelper>>
  fun addFile(fileId: String, metaInfo: Map<FileMetaInfo, Any>, data: ByteArray)
}

enum class FileType {
  PNG,
  JPEG,
  OCTET_STREAM,
  ;

  companion object {
    fun fromMimeType(subContentType: String): FileType {
      return FileType.values()
              .find { it.name.replace("_", "-").equals(subContentType, true) }
              ?: throw IllegalArgumentException("No file type found for: '$subContentType'")
    }
  }

  fun toMimeString(): String {
    return "image/" + this.name.toLowerCase()
  }

}

enum class FileMetaInfo {
  FILE_TYPE, //Must be of Type "FileType"
  FILE_NAME, //Must be of type String
  CREATION_DATE, //when is a picture taken?
  LOCATION //where is a picture taken?
}