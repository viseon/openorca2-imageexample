package util

import ch.viseon.orca2.common.util.File
import com.google.common.hash.Hashing


actual object PlatformFileUtil {
  actual fun sha256(fileData: ByteArray): String {
    return Hashing
            .sha256()
            .hashBytes(fileData)
            .toString()
  }

  actual fun createFile(data: ByteArray, fileName: String, fileType: String): File {
    return File(data, fileType, fileName)
  }
}

