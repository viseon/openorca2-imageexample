package ch.viseon.orca2.example.swing

import ch.viseon.orca2.common.OrcaRun
import ch.viseon.orca2.common.OrcaSource
import ch.viseon.orca2.common.network.NetworkConnection
import ch.viseon.orca2.example.controller.Controller
import ch.viseon.orca2.example.controller.Models
import javax.swing.SwingUtilities
import javax.swing.UIManager

fun main(args: Array<String>) {
    SwingUtilities.invokeLater {
        try {
            for (info in UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus" == info.name) {
                    UIManager.setLookAndFeel(info.className)
                    break
                }
            }
        } catch (e: Exception) {
            // If Nimbus is not available, you can set the GUI to another look and feel.
        }

        runOrcaUi()
    }
}

private fun runOrcaUi() {
    val orcaRun = OrcaRun(OrcaSource(Models.modelStore))

    val mainFrame = MainFrame(orcaRun)
    mainFrame.show()

    val networkConnection = NetworkConnection(Controller.serverUrl)
    Controller.initialize(orcaRun, networkConnection)
}