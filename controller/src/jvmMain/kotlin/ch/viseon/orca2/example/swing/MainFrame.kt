package ch.viseon.orca2.example.swing

import ch.viseon.orca2.common.*
import ch.viseon.orca2.example.controller.LoginViewIds
import ch.viseon.orca2.example.common.controller.ViewPm
import ch.viseon.orca2.example.common.controller.asModelId
import ch.viseon.orca2.example.ui.swing.AbstractMainFrame
import ch.viseon.orca2.rx.Observable

class MainFrame(orcaRun: OrcaRun) : AbstractMainFrame(orcaRun) {

    override fun registerRoutingEvents(orcaSource: OrcaSource): Observable<List<CommandData>> {
        return orcaSource
            .observeProperty(ViewPm.ID, ViewPm.PROP_ROUTE)
            .map {
                val route: String = it.newValue as String

                val componentId = when (route) {
                    "/" -> LoginViewIds.LoginPanel.ID
                    "/main-page" -> "mainPanel".asModelId()
                    else -> ModelId.EMPTY
                }
                if (componentId == ModelId.EMPTY) {
                    println("Component not found for route: $route")
                }
                route to componentId
            }
            .filter { (_, componentType) -> componentType != ModelId.EMPTY }
            .map { (_, componentType) ->
                listOf<CommandData>(CommandUtils.changeValueUI(ViewPm.ID, ViewPm.PROP_COMPONENT, componentType))
            }
    }
}