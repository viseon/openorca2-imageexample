package ch.viseon.orca2.example.controller

import ch.viseon.common.network.EmptyData
import ch.viseon.common.network.JsonData
import ch.viseon.common.network.MultipartData
import ch.viseon.orca2.common.network.NetworkConnection
import ch.viseon.orca2.common.network.Request
import ch.viseon.orca2.common.network.ResponseType
import ch.viseon.orca2.example.dto.AddTagRequest
import ch.viseon.orca2.example.dto.AddTagResponse
import ch.viseon.orca2.example.dto.FileDTO
import ch.viseon.orca2.rx.*
import kotlinx.io.ByteBuffer
import kotlinx.serialization.json.JSON
import kotlinx.serialization.list

class Backend(val networkConnection: NetworkConnection) {

  fun addTag(tagName: String): Observable<AddTagResponse> {
    return networkConnection
            .post(Request("/tag", JsonData(AddTagRequest.serializer(), AddTagRequest(""))))
            .filter { it.isOk }
            .map { JSON.parse(AddTagResponse.serializer(), it.data.toString()) }
  }

  fun removeTag(tagId: Long): Observable<Unit> {
    return networkConnection
            .delete(Request("/tag/$tagId", EmptyData))
            .filter { it.isOk }
            .map { Unit }
  }

  fun getExistingTags(): Observable<List<ch.viseon.orca2.example.dto.Tag>> {
    return networkConnection
            .get(Request("/tag", EmptyData))
            .filter { it.isOk }
            .map {
              val serializer = ch.viseon.orca2.example.dto.Tag.serializer().list
              JSON.parse(serializer, it.data.toString())
            }
  }

  fun uploadPicture(imageFile: List<ImageFile>): Observable<Unit> {
    return ObservableOperators
            .merge(*imageFile
                    .map { file ->
                      file.calculateId().map { it to file }
                    }
                    .toTypedArray()
            )
            .reduceToObservable(MultipartData()) { data, (id, file) ->
              data[id] = file.data
              data
            }
            .map { data ->
              println("reduce operation complete. ")
              Request("/picture", data)
            }
            .flatMap {
              networkConnection
                      .post(it)
                      .map {
                        println("Upload complete")
                        Unit
                      }
            }
  }

    fun loadPictures(): Observable<List<FileDTO>> {
      return networkConnection
              .get(Request("/picture", EmptyData, ResponseType.BINARY))
              .filter { it.isOk }
              .map {
                val buffer = it.data as ByteBuffer
                val noImages = buffer.getInt()
                val images = mutableListOf<FileDTO>()
                for(i in 0 until noImages) {
                  images.add(FileDTO.fromByteBuffer(buffer))
                }
                images
              }
    }

  }

