package ch.viseon.orca2.example.controller

import ch.viseon.orca2.common.util.File
import util.PlatformFileUtil
import ch.viseon.orca2.common.util.useAsByteArray
import ch.viseon.orca2.rx.Observable
import ch.viseon.orca2.rx.ObservableFactory


class ImageFile(val data: File, precalculatedId: String? = null) {

  val name get() = data.name

  val type get() = data.type

  val size: Int get() = data.size

  private lateinit var _id: String

  val id: String get() = _id

  init {
    precalculatedId?.apply {
      _id = this
    }
  }

  fun calculateId(): Observable<String> {
    if (::_id.isInitialized) {
      return ObservableFactory.of(_id)
    }

    return ObservableFactory.create { subscriber ->
      data.useAsByteArray {
        _id = PlatformFileUtil.sha256(it)
        subscriber.onNext(_id)
        subscriber.onCompleted()
      }
    }
  }

}