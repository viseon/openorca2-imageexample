package ch.viseon.orca2.example.controller

import ch.viseon.orca2.binding.components.form.FormPm
import ch.viseon.orca2.binding.components.HTML_TAG
import ch.viseon.orca2.binding.components.panel.PanelPm
import ch.viseon.orca2.common.*
import ch.viseon.orca2.example.common.controller.ViewPm
import ch.viseon.orca2.example.common.controller.asModelId
import ch.viseon.orca2.example.common.controller.asModelType
import ch.viseon.orca2.example.common.controller.asPropertyName
import ch.viseon.orca2.rx.Observable
import ch.viseon.orca2.rx.filter
import ch.viseon.orca2.rx.map


object LoginView {

  fun create(): List<CommandData> {
    return PanelPm.create(LoginViewIds.LoginPanel.ID) {
      add(FormPm.create(LoginViewIds.LoginFormConfig.ID) {

        renderHint(mapOf (HTML_TAG to "loginForm"))

        dataPm(LoginViewIds.LoginFormData.ID, LoginViewIds.LoginFormData.TYPE) {
          property(LoginViewIds.LoginFormData.PROP_USER_NAME) {
            label = "Benutzername"
            value = ""
          }
          property(LoginViewIds.LoginFormData.PROP_PW) {
            label = "Password"
            value = ""
          }
        }
        submitaAction {
          value = "login-form-submit"
          label = "Login"
        }
      })
    }.commands
  }

  fun controller(orcaSource: OrcaSource, backend: Backend): Observable<List<CommandData>> {
    return handleLoginAction(orcaSource)
  }

  private fun handleLoginAction(orcaSource: OrcaSource): Observable<List<CommandData>> {
    return orcaSource
            .registerNamedCommand("login-form-submit")
            .filter {
              val pm = orcaSource.model(LoginViewIds.LoginFormData.ID)
              pm[LoginViewIds.LoginFormData.PROP_PW].get() as String == "123"
            }
            .map {
              println("navigate to Main screen")
              navigateToMainScreen()
            }
  }

  private fun navigateToMainScreen(): List<CommandData> {
    return listOf(
            ChangeValueCommandData(Source.CONTROLLER, ViewPm.ID, ViewPm.PROP_ROUTE, Tag.VALUE, "/main-page")
    )
  }

}

object LoginViewIds {
  object LoginPanel {
    val ID = "loginPanel".asModelId()
  }

  object LoginFormConfig {
    val ID = "loginFormConfig".asModelId()
  }

  object LoginFormData {
    val ID = "loginFormData".asModelId()
    val TYPE = "loginFormData".asModelType()

    val PROP_USER_NAME = "username".asPropertyName()
    val PROP_PW = "pw".asPropertyName()
  }
}