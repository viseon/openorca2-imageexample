package ch.viseon.orca2.example.controller

import ch.viseon.orca2.common.*
import ch.viseon.orca2.common.network.NetworkConnection
import ch.viseon.orca2.example.common.controller.ViewPm
import ch.viseon.orca2.rx.Observable
import ch.viseon.orca2.rx.ObservableOperators

object Controller {

  fun initialize(orcaRun: OrcaRun, networkConnection: NetworkConnection) {
    orcaRun.register {
      registerControllerCommands(it, Backend(networkConnection))
    }
    orcaRun.processCommands(init())
  }

  val serverUrl = "http://localhost:8080"

  private fun registerControllerCommands(orcaSource: OrcaSource, backend: Backend): Observable<List<CommandData>> {
    return ObservableOperators
            .merge(
                LoginView.controller(orcaSource, backend),
                MainView.controller(orcaSource, backend)
            )
  }

  private fun init(): List<CommandData> {
    return ViewPm.create().commands
            .plus(MainView.create())
            .plus(LoginView.create())
            .plus(ViewPm.changeRouteController("/"))
  }

}

object Models {

  val modelStore = DefaultPresentationModelStore()

}

