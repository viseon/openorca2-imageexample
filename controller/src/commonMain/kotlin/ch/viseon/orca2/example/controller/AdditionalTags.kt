package ch.viseon.orca2.example.controller

import ch.viseon.orca2.common.Tag


object AdditionalTags {
  val PROPERTY_TYPE = Tag("property-TYPE")
}