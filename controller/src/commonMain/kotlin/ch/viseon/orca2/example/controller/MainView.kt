package ch.viseon.orca2.example.controller

import ch.viseon.orca2.binding.ToolBarPm
import ch.viseon.orca2.binding.components.button.ButtonPm
import ch.viseon.orca2.binding.components.file.FileSelectionActionPm
import ch.viseon.orca2.binding.components.file.FileUploadResultPm
import ch.viseon.orca2.binding.components.grid.GridPm
import ch.viseon.orca2.binding.components.image.ImageViewerElementPm
import ch.viseon.orca2.binding.components.image.ImageViewerPm
import ch.viseon.orca2.binding.components.panel.PanelLayout
import ch.viseon.orca2.binding.components.panel.PanelPm
import ch.viseon.orca2.common.*
import ch.viseon.orca2.common.util.File
import ch.viseon.orca2.common.util.myPrintStackTrace
import ch.viseon.orca2.example.common.controller.ViewPm
import ch.viseon.orca2.example.common.controller.asModelId
import ch.viseon.orca2.example.common.controller.asModelType
import ch.viseon.orca2.example.common.controller.asPropertyName
import ch.viseon.orca2.rx.*
import util.PlatformFileUtil


private val ADD_TAG_BUTTON_MODEL_ID = "add-tag-button".asModelId()
private val REMOVE_TAG_MODEL_ID = "remove-tag-button".asModelId()
private val TAG_LIST_MODEL_ID = "tag-list".asModelId()
private val TAG_ROW_MODEL_ID = "tag-row".asModelId()
private val TAG_ROW_MODEL_TYPE = "tag-row".asModelType()

private const val ADD_TAG_COMMAND = "add-tag"
private const val REMOVE_TAG_COMMAND = "remove-tag"
private const val UPLOAD_PICTURE_COMMAND = "upload-picture"

private val UPLOAD_PICUTE_ID = "upload-picture".asModelId()

object MainView {

  private val ID = "mainPanel".asModelId()

  fun create(): List<CommandData> {
    return PanelPm.create(ID) {
      add(ToolBarPm.create("toolbar".asModelId()) {
        add(ButtonPm.create(ADD_TAG_BUTTON_MODEL_ID) {
          configure {
            label = "Add Tag"
            value = ADD_TAG_COMMAND
          }
        })
        add(ButtonPm.create(REMOVE_TAG_MODEL_ID) {
          configure {
            label = "Remove Tag"
            value = REMOVE_TAG_COMMAND
          }
        })
        add(FileSelectionActionPm.create(UPLOAD_PICUTE_ID) {
          requestUrl {
            value = "/picture"
          }
          configureUploadButton {
            label = "Upload Test Picture"
            value = UPLOAD_PICTURE_COMMAND
          }
        })
      })
      add(PanelPm.create("content".asModelId()) {
        layout = PanelLayout.HORIZONTAL
        add(PanelPm.create("tag-list-panel".asModelId()) {
          add(GridPm.create(TAG_LIST_MODEL_ID) {
            metaInfo("tag-list-meta".asModelId(), "tag-list-meta".asModelType()) {
              property("id".asPropertyName()) {
                label = "ID"
              }
              property("name".asPropertyName()) {
                label = "Label"
              }
            }
            row(TAG_ROW_MODEL_TYPE)
          })
        })
        add(ImageViewerPm.create("pictureViewer".asModelId()))
      })
    }
            .commands
  }

  fun controller(orcaSource: OrcaSource, backend: Backend): Observable<List<CommandData>> {
    return ObservableOperators.merge(
            loadExistingTags(orcaSource, backend),
            loadExistingPictures(orcaSource, backend),
            addTag(orcaSource, backend),
            removeTag(orcaSource, backend),
            uploadPicture(orcaSource, backend)
    )
  }

  private fun uploadPicture(orcaSource: OrcaSource, backend: Backend): Observable<List<CommandData>> {
    return orcaSource
            .registerNamedCommand(UPLOAD_PICTURE_COMMAND)
            .flatMap { actionEvent ->
              orcaSource.model(actionEvent.pmIds.first())
                      .let { presentationModel ->
                        val files: List<File> = presentationModel[FileUploadResultPm.PROP_FILE_DATA].getValue()
                        files.map { ImageFile(it) }
                      }
                      .let { data ->
                        backend
                                .uploadPicture(data)
                                .map { _ ->
                                  data.map { createPicturePm(it) }
                                }
                      }
            }
  }

  private fun removeTag(orcaSource: OrcaSource, backend: Backend): Observable<List<CommandData>> {
    return orcaSource
            .registerNamedCommand(REMOVE_TAG_COMMAND)
            .flatMap { _ ->
              val selectedTagPm = getSelectedTagPm(orcaSource, TAG_LIST_MODEL_ID)
              selectedTagPm?.let { _ ->
                backend
                        .removeTag(selectedTagPm["id".asPropertyName()].getValue())
                        .map {
                          var modelCommands = listOf<CommandData>(
                                  RemoveModelCommandData(Source.CONTROLLER, selectedTagPm),
                                  RemoveModelCommandData(Source.CONTROLLER,
                                          GridPm.getSelectionModelId(selectedTagPm.id),
                                          GridPm.getSelectionType(TAG_LIST_MODEL_ID)
                                  )
                          )
                          if (orcaSource.contains(TAG_ROW_MODEL_TYPE)) {
                            val nextToSelect = orcaSource.models(TAG_ROW_MODEL_TYPE).first()
                            modelCommands = modelCommands.plus(GridPm.createSelectionPm(nextToSelect.id, GridPm.getSelectionType(
                                TAG_LIST_MODEL_ID
                            ), Source.CONTROLLER))
                          }
                          modelCommands
                        }
              } ?: ObservableFactory.empty<List<CommandData>>()
            }
  }

  private fun addTag(orcaSource: OrcaSource, backend: Backend): Observable<List<CommandData>> {
    return orcaSource
            .registerNamedCommand(ADD_TAG_COMMAND)
            .flatMap {
              val tagName = "tag ${orcaSource.getAllModels().size - 1}"
              backend
                      .addTag(tagName)
                      .map { response ->
                        listOf(createRowPm(response.id, tagName))
                      }
            }
  }

  private fun isPageVisible(orcaSource: OrcaSource): Observable<Unit> {
    return ViewPm
            .isComponentVisible(orcaSource, ID)
  }

  private fun loadExistingPictures(orcaSource: OrcaSource, backend: Backend): Observable<List<CommandData>> {
    return isPageVisible(orcaSource)
            .flatMap {
              backend
                      .loadPictures()
                      .onErrorResumeNext { throwable: Throwable ->
                        myPrintStackTrace(throwable)
                        ObservableFactory.of(listOf())
                      }
                      .map { files ->
                        files.map { fileDto ->
                          val data = fileDto.data
                          val file = PlatformFileUtil.createFile(data, fileDto.fileName, fileDto.type)
                          val imageFile = ImageFile(file, fileDto.id)
                          val result = createPicturePm(imageFile)
                          result
                        }
                      }
            }
  }

  private fun loadExistingTags(orcaSource: OrcaSource, backend: Backend): Observable<List<CommandData>> {
    return isPageVisible(orcaSource)
            .flatMap {
              backend
                      .getExistingTags()
                      .map { tags ->
                        tags.map { tag -> createRowPm(tag.id, tag.tagName) }
                      }
            }
  }

  private fun getSelectedTagPm(orcaSource: OrcaSource, configModelId: ModelId): PresentationModel? {
    return orcaSource.models(GridPm.getSelectionType(configModelId))
            .takeIf { it.isNotEmpty() }
            ?.first()
            ?.let { orcaSource.model(it[GridPm.PROP_SELECTED_MODEL_ID].getValue()) }
  }

  private fun createRowPm(tagId: Long, tagLabel: String): CommandData {
    return PresentationModelBuilder("${TAG_ROW_MODEL_ID.stringId}$tagId".asModelId(), TAG_ROW_MODEL_TYPE) {
      property("id".asPropertyName()) {
        value = tagId
      }
      property("name".asPropertyName()) {
        value = tagLabel
      }
    }
            .build(Source.CONTROLLER)
  }

  private fun createPicturePm(imageFile: ImageFile): CommandData {
    return ImageViewerElementPm.create(imageFile.id.asModelId(), imageFile.data).commands.first()
  }

}