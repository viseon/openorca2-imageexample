package ch.viseon.orca2.example.controller


/**
 * Holds a reference to files.
 */
class FileStore {

  private val files = mutableMapOf<String, ByteArray>()

  fun putFile(fileName: String, data: ByteArray) {
    files[fileName] = data
  }

  operator fun get(fileName: String): ByteArray {
    return files[fileName] ?: throw IllegalArgumentException("file not present: '$fileName'")
  }

  fun getFileBase64(fileName: String): ByteArray {

    return this[fileName]
  }

}

