package util

import ch.viseon.orca2.common.util.File


expect object PlatformFileUtil {

  fun sha256(fileData: ByteArray): String

  fun createFile(data: ByteArray, fileName: String, fileType: String): File

}



