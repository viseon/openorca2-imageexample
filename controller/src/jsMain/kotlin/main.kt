import ch.viseon.orca2.common.*
import ch.viseon.orca2.common.network.NetworkConnection
import ch.viseon.orca2.example.common.controller.*
import ch.viseon.orca2.example.controller.Controller
import ch.viseon.orca2.example.controller.LoginViewIds
import ch.viseon.orca2.example.controller.Models
import ch.viseon.orca2.rx.*
import org.w3c.dom.PopStateEvent
import kotlin.browser.document
import kotlin.browser.window
import kotlin.dom.clear

val orcaRun = OrcaRun(OrcaSource(Models.modelStore))


fun main(args: Array<String>) {
//  require("style.css")
  require("buffer")

  val networkConnection = NetworkConnection(Controller.serverUrl)

  window.onload = {
    val bindingContext = BindingContext(orcaRun, networkConnection)
    orcaRun.register {
      registerBrowserEvents(bindingContext)
      registerEvents(bindingContext)
    }
    Controller.initialize(orcaRun, networkConnection)
  }
}

fun registerBrowserEvents(bindingContext: BindingContext) {
  window.onpopstate = {
    val popStateEvent = it as PopStateEvent
    popStateEvent.state?.let { route ->
      val commands = listOf(CommandUtils.changeValueUI(ViewPm.ID, ViewPm.PROP_ROUTE, route))
      bindingContext.orcaRun.processCommands(commands)
    }
    Unit
  }

}

fun registerEvents(bindingContext: BindingContext): Observable<List<CommandData>> {
  return ObservableOperators
          .merge(
                  bindingContext.orcaSource
                          .observeProperty(ViewPm.ID, ViewPm.PROP_ROUTE)
                          .map {
                            val route: String = it.newValue as String

                            if (it.source.isController()) {
                              window.history.pushState(route, "", route)
                            }

                            val componentId = when (route) {
                              "/" -> LoginViewIds.LoginPanel.ID
                              "/main-page" -> "mainPanel".asModelId()
                              else -> ModelId.EMPTY
                            }
                            if (componentId == ModelId.EMPTY) {
                              println("Component not found for route: $route")
                            }
                            route to componentId
                          }
                          .filter { (_, componentType) -> componentType != ModelId.EMPTY }
                          .map { (_, componentType) ->
                            listOf<CommandData>(CommandUtils.changeValueUI(ViewPm.ID, ViewPm.PROP_COMPONENT, componentType))
                          },
                  ObservableOperators
                          .merge(
                                  bindingContext.orcaSource
                                          .observeProperty(ViewPm.ID, ViewPm.PROP_COMPONENT)
                                          .map { it.source to it.newValue as ModelId },
                                  //Init handling
                                  bindingContext.orcaSource
                                          .observeModelStore()
                                          .filter { it.eventType.isAdd() }
                                          .filter { it.modelType == ViewPm.TYPE }
                                          .filter { it.modelId != ModelId.EMPTY }
                                          .map { bindingContext.orcaSource.model(it.modelId) }
                                          .map { Source.CONTROLLER to it[ViewPm.PROP_COMPONENT].getValue() as ModelId }
                          )
                          .filter { (_, id) -> id != ModelId.EMPTY }
                          .flatMap { (source, id) ->

                            val model = bindingContext.orcaSource.model(id)
                            document.body?.clear()
                            //History api
                            ComponentFactory.createComponent(model, bindingContext, document.body!!)
                          }
          )
}

