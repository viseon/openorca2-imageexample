package util

import ch.viseon.orca2.common.util.File
import org.w3c.files.FilePropertyBag

actual object PlatformFileUtil {

  actual fun sha256(fileData: ByteArray): String {
    val hashBytes = sjcl.hash.sha256.hash(fileData)
    return sjcl.codec.hex.fromBits(hashBytes)
  }

  actual fun createFile(data: ByteArray, fileName: String, fileType: String): File {
    return org.w3c.files.File(arrayOf(data.asDynamic()), fileName, FilePropertyBag(type = fileType))
  }
}

