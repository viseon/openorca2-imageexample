@file:JsModule("sjcl")
@file:JsQualifier("codec.hex")

package sjcl.codec.hex

/**
 * @param {BitArray} array
 */
external fun fromBits(array: dynamic): String

external fun toBits(string: String): dynamic



