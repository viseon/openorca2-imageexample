package ch.viseon.orca2.example.dto

import kotlinx.serialization.Serializable


@Serializable
class DeleteTagRequest(val id: Long) {

}